class Controller {
  static setup () {
    //console.log(rwc_json)
    var rwc = new RWC(2019)
    let team_names = rwc_json["teams"]
    for (let new_name of team_names) {
      //console.log(new_name)
      rwc.add_team(new_name)
    }
    //console.log(Object.keys(rwc_json["pools"]))
    for (let new_name of Object.keys(rwc_json["pools"])) {
      //console.log(new_name)
      let a_pool = rwc.add_pool(new_name)
      for (let existing_team_name of rwc_json["pools"][new_name]) {
        //console.log('\t', existing_team_name)
        let a_team = rwc.find_team(existing_team_name)
        a_pool.add_team(a_team)
      }
    }
    for (let match of rwc_json["round-robin"]) {
      let pool_name = match['pool']
      let the_pool = rwc.find_pool(pool_name)
      let new_when = match['when'] //datetime.strptime(match['when'], '%d %B %Y')
      let existing_team_a_name = match['team_a']
      let existing_team_b_name = match['team_b']
      let venue_name = match['venue']
      let city = match['city']
      //console.log(new_when, existing_team_a_name, existing_team_b_name, venue_name, city)
      let the_team_a = rwc.find_team(existing_team_a_name)
      let the_team_b = rwc.find_team(existing_team_b_name)
      let the_venue = rwc.add_venue_if_new(venue_name, city)
      let a_match = rwc.add_match(new_when, the_team_a,
        the_team_b, the_venue, the_pool)
        the_pool.add_match(a_match)
      }
      return rwc
    }
  }
