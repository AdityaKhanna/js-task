# js-task

This task is to let you gain experience so you can

- Design, code and test a program.
- Create test data and import real-world data.
- Write software that passes automated tests.
- Plan and document how you went about developing the software.
- Explain how the code and the designs are fit-for-purpose.

This assignment involves creating a results recording application for the 2019 Rugby World Cup.
You will be given data about the teams, pools, venues and matches. You will have to add data about
match results as they are played. 

See [controller.js](controller.js) for a demo of how to display match data. Use the [index.html](index.html) code to load and run your JavaScript
program.

You wil also need the provided json.js file which hold the actual data.

### Writing Source Code
Create directories and build the necessary classes. Look at the provided [index.html](index.html) code on the previous pages to see the required
directory and file structure.
Look at the data displayed by [controller.js](controller.js) to work out what attributes are needed in classes.

Plan this by drawing a UML package diagram. http://www.agilemodeling.com/artifacts/packageDiagram.htm

Write constructor functions for the classes.

#### Tasks

1. Create base Rugby World Cup object
2. Add Teams
3. Display Teams in alphabet order
4. Add Pools
5. Display Pools
6. Link Teams to Pools
7. Display Teams by Pool
8. Add Matches (and venues)
9. Display matches by date
10. Display matches by Venue
11. Display match by Team
12. Add results for a Match
13. Display results for a Match
14. Calculate points
15. Display points by Team

For each task listed in section B, draw a design level UML class diagram and a UML sequence diagram of how the code works.

On the display side, you can produce a display for any of the following:
• Display Teams in alphabet order
• Display Pools
• Display Teams by pool
• Display matches by date
• Display matches by venue
• Display matches by team
• Display results for a match
• Display points by team

### Writing Unit Tests
For the tasks listed above, create unit tests that establish that the code is working correctly. write Unit Tests using the **Jasmine** testing framework